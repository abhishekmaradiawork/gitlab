import { s__ } from '~/locale';

export const GENERAL_ERROR_MESSAGE = s__(
  'PurchaseStep|An error occurred in the purchase step. If the problem persists please contact support@gitlab.com.',
);
